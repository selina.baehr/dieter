use image::io::Reader as ImageReader;
use image::*;
use std::env;
use std::error::Error;
use std::cmp::{min, max};
fn main() -> Result<(), Box<dyn Error>> {
    println!("Hello, world!");
    let args: Vec<String> = env::args().collect();
    // dbg!(args);
    let img = ImageReader::open(&args[1])?.decode()?;
    let pimg = error_diffusion(img, 100);
    pimg.save("test4.jpg")?;
    Ok(())
}
fn apply_threshold(img: DynamicImage, t: u8) -> DynamicImage {
    let mut luma = img.into_luma8();
    for (_, _, Luma([pixel])) in luma.enumerate_pixels_mut() {
        *pixel = if *pixel > t { 255 } else { 0 }
        // dbg!(pixel);
    }
    DynamicImage::ImageLuma8(luma)
}
fn error_diffusion(img: DynamicImage, t: u8) -> DynamicImage {
    let mut luma = img.into_luma8();
    for y in 0..luma.height() {
        for x in 0..luma.width() {
            let Luma([pixel]) = luma.get_pixel(x, y).clone();
            let new_pixel = if pixel > t { 255 } else { 0 };
            luma.put_pixel(x, y, Luma([new_pixel]));
            let error = pixel as i32 - new_pixel  as i32;
            // dbg!(error);
            if let Some(Luma([rightpixel])) = luma.get_pixel_mut_checked(x + 1, y) {
                *rightpixel = (max(0, min(255, *rightpixel as i32 + error / 2))) as u8;
            }
            if let Some(Luma([downpixel])) = luma.get_pixel_mut_checked(x, y + 1) {
                *downpixel = (max(0, min(255, *downpixel as i32 + error / 2))) as u8;
            }
            // dbg!(pixel);
        }
    }
    DynamicImage::ImageLuma8(luma)
}
